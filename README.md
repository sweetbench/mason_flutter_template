# Flutter Builder

![](https://gitlab.com/sweetbench/flutter_builder/badges/dev/pipeline.svg?style=flat)
![](https://gitlab.com/sweetbench/flutter_builder/badges/dev/coverage.svg?style=flat)
![](https://gitlab.com/sweetbench/flutter_builder/-/badges/release.svg?key_text=latest+release&style=flat)
![](https://img.shields.io/gitlab/license/sweetbench/flutter_builder?style=flat&color=yellowgreen)

This project is a [mason](https://pub.dev/packages/mason) brick containing a ready-to-go new [Flutter](https://flutter.dev/) application.

The template is made alongside the [Code Best Practices](https://docs.flutter.dev/perf/best-practices), [Happy Paths](https://docs.flutter.dev/development/packages-and-plugins/happy-paths) and [Flutter Favorite](https://pub.dev/packages?q=is%3Aflutter-favorite&sort=popularity) packages.

## Usage

1. Install [mason_cli](https://pub.dev/packages/mason_cli) package.

``` bash
dart pub global activate mason_cli
```

2. Adds a brick from a [git](https://git-scm.com/) repository.

``` bash
mason add --git-url https://gitlab.com/sweetbench/flutter_builder flutter_builder --global
```

3. Lists installed [bricks](https://brickhub.dev/).

``` bash
mason list --global
```

``` bash
└── flutter_builder 0.1.0+1 -> https://gitlab.com/sweetbench/flutter_builder
```

4. Generate code using an existing brick template.

``` bash
mason make flutter_builder
```

## Roadmap

- [ ] [Minimum viable product](https://gitlab.com/sweetbench/flutter_builder/-/milestones/1)
- [ ] [Local storage](https://gitlab.com/sweetbench/flutter_builder/-/milestones/2)
- [ ] [Firebase integration](https://gitlab.com/sweetbench/flutter_builder/-/milestones/3)
- [ ] [State management](https://gitlab.com/sweetbench/flutter_builder/-/milestones/4)

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Authors

 - [Jan Kozera](https://gitlab.com/jan-koz)
 - [Piotr Rozpończyk](https://gitlab.com/rozpo)
