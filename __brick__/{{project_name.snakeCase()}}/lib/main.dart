import 'package:flutter/material.dart';

import 'package:{{project_name.snakeCase()}}/core/page/page_core.dart';
import 'package:{{project_name.snakeCase()}}/core/locale/locale_core.dart';
import 'package:{{project_name.snakeCase()}}/core/router/router_core.dart';
import 'package:{{project_name.snakeCase()}}/core/theme/core_colors.dart';
import 'package:{{project_name.snakeCase()}}/utils/url_strategy/url_strategy.dart';

void main() {
  usePathUrlStrategy();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: const ColorScheme.light(
          primary: CoreColors.primary,
          secondary: CoreColors.secondary,
        ),
      ),
      localizationsDelegates: LocaleCore.localizationsDelegates,
      supportedLocales: LocaleCore.supportedLocales,
      onGenerateRoute: RouterCore.generateRoute,
      initialRoute: HomePage.routeName,
    );
  }
}
