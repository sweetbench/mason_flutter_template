import 'package:flutter/material.dart';

mixin CoreColors {
  static const Color primary = Colors.{{primary_color.camelCase()}};
  static const Color secondary = Colors.{{secondary_color.camelCase()}};
}
