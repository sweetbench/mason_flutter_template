import 'package:flutter/material.dart';

import 'package:{{project_name.snakeCase()}}/core/page/page_core.dart';

mixin RouterCore {
  static Route<dynamic> generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case HomePage.routeName:
        return _buildRoute(
          name: HomePage.routeName,
          page: const HomePage(),
        );
      default:
        return _buildRoute(
          name: routeSettings.name,
          page: const NotFoundPage(),
        );
    }
  }

  static PageRouteBuilder<dynamic> _buildRoute({
    String? name,
    required Widget page,
  }) {
    return PageRouteBuilder(
      settings: RouteSettings(name: name),
      pageBuilder: (_, __, ___) {
        return page;
      },
    );
  }
}
