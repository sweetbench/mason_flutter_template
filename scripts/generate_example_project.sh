#!/bin/bash

if [ ! -f mason.yaml ]; then
    mason init
    mason add --path ../. flutter_builder
fi

mason make flutter_builder --output-dir .. --config-path configs/example_config.json --on-conflict overwrite

rm mason.yaml
rm mason-lock.json
rm -r .mason
