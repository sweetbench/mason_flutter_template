if (!(Test-Path mason.yaml -PathType Leaf)) {
    mason.bat init
    mason.bat add --path ../. flutter_builder
} 

mason.bat make flutter_builder --output-dir .. --config-path configs/example_config.json --on-conflict overwrite

Remove-Item mason.yaml
Remove-Item mason-lock.json
Remove-Item -r .mason
