import 'dart:io';

import 'package:mason/mason.dart';

const String kWorkingDirectory = './{{project_name.snakeCase()}}';

class Command {
  final String name;
  final List<String> args;
  final String workingDirectory;

  Command(
    this.name, {
    this.args = const [],
    this.workingDirectory = kWorkingDirectory,
  });
}

// List of commands to execute after mason brick generation finishes.
final List<Command> commandList = [
  Command('flutter gen-l10n'),
  Command('flutter pub upgrade'),
];

void run(HookContext context) {
  context.logger.info(
    '-----\n'
    'Generate additional files...',
  );

  final String platforms = '{{platforms.lowerCase()}}';
  final String availablePlatforms =
      platforms.substring(1, platforms.length - 1).replaceAll(' ', '').trim();

  context.logger.info(
    'Activating following platforms: $platforms',
  );

  commandList.add(
    Command('flutter create .', args: ['--platforms=$availablePlatforms']),
  );

  addGenerateLicenseCommand(context);

  for (var command in commandList) {
    var result = Process.runSync(
      workingDirectory: command.workingDirectory,
      runInShell: true,
      command.name,
      command.args,
    );

    stdout.write(result.stdout);
    stderr.write(result.stderr);
  }
}

void _generateApacheLicense() {
  final String project_name = '{{project_name}}';
  final String year = DateTime.now().year.toString();

  commandList.addAll([
    Command('cp ../assets/licenses/apache_license LICENSE'),
    Command('sed -i \'s/FULLNAME/$project_name/g\' LICENSE'),
    Command('sed -i \'s/YEAR/$year/g\' LICENSE'),
  ]);
}

void _generateGnuLicense() {
  final String project_name = '{{project_name}}';
  final String organization_name = '{{organization_name}}';
  final String year = DateTime.now().year.toString();

  commandList.addAll([
    Command('cp ../assets/licenses/gnu_license LICENSE'),
    Command('sed -i \'s/FULLNAME/$project_name/g\' LICENSE'),
    Command('sed -i \'s/ORGANIZATION/$organization_name/g\' LICENSE'),
    Command('sed -i \'s/YEAR/$year/g\' LICENSE'),
  ]);
}

void _generateMitLicense() {
  final String project_name = '{{project_name}}';
  final String year = DateTime.now().year.toString();

  commandList.addAll([
    Command('cp ../assets/licenses/mit_license LICENSE'),
    Command('sed -i \'s/FULLNAME/$project_name/g\' LICENSE'),
    Command('sed -i \'s/YEAR/$year/g\' LICENSE'),
  ]);
}

void _generateMozillaLicense() {
  commandList.addAll([
    Command('cp ../assets/licenses/mozilla_license LICENSE'),
  ]);
}

void addGenerateLicenseCommand(HookContext context) {
  switch ('{{license_type}}') {
    case 'Apache License 2.0':
      _generateApacheLicense();
      break;
    case 'GNU General Public License v3.0':
      _generateGnuLicense();
      break;
    case 'MIT License':
      _generateMitLicense();
      break;
    case 'Mozilla Public License 2.0':
        _generateMozillaLicense();
        break;
    default:
      context.logger.alert(
        'License type: {{license_type}}, file not generated.'
        ' Please create LICENSE file manually.',
      );
      break;
  }
}
